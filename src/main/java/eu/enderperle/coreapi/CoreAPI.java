package eu.enderperle.coreapi;

import eu.enderperle.coreapi.core.commands.Vanish;
import eu.enderperle.coreapi.modules.EventStuff;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

public class CoreAPI extends JavaPlugin {

    private static CoreAPI instance;
    public static Scoreboard mainSB;

    @Override
    public void onEnable() {
        instance = this;

        getServer().getPluginManager().registerEvents(new EventStuff(), this);
        getCommand("vanish").setExecutor(new Vanish());

        mainSB = Bukkit.getScoreboardManager().getNewScoreboard();
        mainSB.registerNewTeam("aVanish").setPrefix("§7Vanish §7| §7");
        mainSB.registerNewTeam("bAdmin").setPrefix("§4Admin §7| §4");
        mainSB.registerNewTeam("cDev").setPrefix("§bDev §7| §b");
        mainSB.registerNewTeam("dSrMod").setPrefix("§cSrMod §7| §c");
        mainSB.registerNewTeam("eMod").setPrefix("§cMod §7| §c");
        mainSB.registerNewTeam("eTMod").setPrefix("§cTMod §7| §c");
        mainSB.registerNewTeam("fSup").setPrefix("§9Sup §7| §9");
        mainSB.registerNewTeam("gTSup").setPrefix("§9TSup §7| §9");
        mainSB.registerNewTeam("hBuilder").setPrefix("§3Builder §7| §3");
        mainSB.registerNewTeam("iVIP").setPrefix("§5VIP §7| §5");
        mainSB.registerNewTeam("jPremiumPlus").setPrefix("§e");
        mainSB.registerNewTeam("lPremium").setPrefix("§6");
        mainSB.registerNewTeam("mUser").setPrefix("§a");
        mainSB.registerNewTeam("nHacker").setPrefix("§7[§4✘§7] §d");

    }

    @Override
    public void onDisable() {
    }

    public static CoreAPI getInstance() {
        return instance;
    }
}
