package eu.enderperle.coreapi.modules.countdown;

import eu.enderperle.coreapi.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

public class CountDown {

    private CoreAPI coreAPI = CoreAPI.getInstance();

    private int current;
    private int end;
    private int taskid;

    //new CountDown(30, 0).start();
    public CountDown(int startTime, int endTime){
        this.current = startTime;
        this.end = endTime;
        start();
    }

    @SuppressWarnings("deprecation")
    public void start(){
        taskid = Bukkit.getScheduler().scheduleAsyncRepeatingTask(coreAPI, () -> {
            if(current >= end) Bukkit.getScheduler().cancelTask(taskid);
            if(end > current) Bukkit.getScheduler().cancelTask(taskid);
            current--;
        }, 20, 20);

    }

    public boolean isStarted(){
        return current != 0;
    }

    public void stop(){
        Bukkit.getScheduler().cancelTask(taskid);
        current = 0;
    }

    public Integer getCurrent(){
        return current;
    }



}
