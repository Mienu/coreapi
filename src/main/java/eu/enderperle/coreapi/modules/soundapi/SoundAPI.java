package eu.enderperle.coreapi.modules.soundapi;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundAPI {

    private Player player;

    public SoundAPI(Player player){
        this.player = player;
    }

    public void playSoundPlayer(Sound soundtype ){
        player.playSound(player.getLocation(), soundtype, 100,1);
    }

    public void broadcastSoundNearPlayer(Sound soundtype){
        Bukkit.getWorld(player.getWorld().getName()).playSound(player.getLocation(), soundtype, 100,1);
    }
    public void broadcastSound(Sound soundtype){
        for(Player all : Bukkit.getOnlinePlayers()){
            all.playSound(all.getLocation(), soundtype, 100,1);
        }
    }


}

