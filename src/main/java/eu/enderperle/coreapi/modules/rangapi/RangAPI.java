package eu.enderperle.coreapi.modules.rangapi;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.dytanic.cloudnet.lib.player.OfflinePlayer;
import de.dytanic.cloudnet.lib.player.permission.PermissionEntity;
import eu.enderperle.coreapi.CoreAPI;
import eu.enderperle.coreapi.core.commands.Vanish;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

public class RangAPI {

    private Scoreboard sb = CoreAPI.mainSB;
    private CloudPlayer player;
    public Player p;

    public RangAPI(Player p){
        this.player = CloudAPI.getInstance().getOnlinePlayer(player.getUniqueId());
        this.p = p;
    }

    public void setRang(){
        PermissionEntity permissionEntity = player.getPermissionEntity();

        if(permissionEntity.isInGroup("Admin")){
            sb.getTeam("bAdmin").addEntry(p.getName());
            p.setDisplayName("§4" + p.getName());
        }else if(permissionEntity.isInGroup("Dev")){
            sb.getTeam("cDev").addEntry(p.getName());
            p.setDisplayName("§b" + p.getName());
        }else if(permissionEntity.isInGroup("SrMod")){
            sb.getTeam("dSrMod").addEntry(p.getName());
            p.setDisplayName("§c" + p.getName());
        }else if(permissionEntity.isInGroup("Mod")){
            sb.getTeam("eMod").addEntry(p.getName());
            p.setDisplayName(("§c") + p.getName());
        }else if(permissionEntity.isInGroup("TMod")){
            sb.getTeam("fTMod").addEntry(p.getName());
            p.setDisplayName("§c" + p.getName());
        }else if(permissionEntity.isInGroup("Sup")){
            sb.getTeam("gSup").addEntry(p.getName());
            p.setDisplayName("§9" + p.getName());
        }else if(permissionEntity.isInGroup("TSup")){
            sb.getTeam("hTSup").addEntry(p.getName());
            p.setDisplayName("§9" + p.getName());
        }else if(permissionEntity.isInGroup("Builder")){
            sb.getTeam("iBuilder").addEntry(p.getName());
            p.setDisplayName("§3" + p.getName());
        }else if(permissionEntity.isInGroup("VIP")){
            sb.getTeam("jVIP").addEntry(p.getName());
            p.setDisplayName("§5" + p.getName());
        }else if(permissionEntity.isInGroup("PremiumPlus")){
            sb.getTeam("kPremiumPlus").addEntry(p.getName());
            p.setDisplayName("§e" + p.getName());
        }else if(permissionEntity.isInGroup("Premium")){
            sb.getTeam("lPremium").addEntry(p.getName());
            p.setDisplayName("§6" + p.getName());
        }else if(permissionEntity.isInGroup("User")){
            sb.getTeam("mUser").addEntry(p.getName());
            p.setDisplayName("a" + p.getName());
        }else if(permissionEntity.isInGroup("Hacker")){
            sb.getTeam("nHacker").addEntry(p.getName());
            p.setDisplayName("§d" + p.getName());
        }else if(new Vanish(p).isVanished()){
            sb.getTeam("aVanish").addEntry(p.getName());
        }else{
            sb.getTeam("mUser").addEntry(p.getName());
            p.setDisplayName("a" + p.getName());
        }

        Bukkit.getOnlinePlayers().forEach(all -> all.setScoreboard(sb));
    }

}
