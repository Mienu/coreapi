package eu.enderperle.coreapi.modules.nickapi;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import eu.enderperle.coreapi.CoreAPI;
import eu.enderperle.coreapi.modules.uuidfetcher.GameProfileBuilder;
import eu.enderperle.coreapi.modules.uuidfetcher.UUIDFetcher;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.util.CraftChatMessage;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NickAPI {


    public void changeSkin(CraftPlayer player, String nickname){
        GameProfile gp = player.getProfile();
        try {
            gp = GameProfileBuilder.fetch(UUIDFetcher.getUUID(nickname));
        } catch (IOException e) {
            e.printStackTrace();
            player.sendMessage("§cSkinchange failure§7!");
            return;
        }


        Collection<Property> probs = gp.getProperties().get("textures");
        player.getHandle().getProfile().getProperties().removeAll("textures");
        player.getHandle().getProfile().getProperties().putAll("textures", probs);
        PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(player.getEntityId());
        sendPacket(destroy);
        PacketPlayOutPlayerInfo tabremove = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, player.getHandle());
        sendPacket(tabremove);
        player.getHandle().setHealth(0);

        new BukkitRunnable(){
            @Override
            public void run() {
                player.spigot().respawn();
                PacketPlayOutPlayerInfo tabadd = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, player.getHandle());
                sendPacket(tabadd);
                PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(player.getHandle());
                for(Player all : Bukkit.getOnlinePlayers()){
                    if(!all.getName().equals(player.getName())){
                        ((CraftPlayer)all).getHandle().playerConnection.sendPacket(spawn);
                    }
                }
            }
        }.runTaskLater(CoreAPI.getInstance(), 3);

    }

    public void sendPacket(Packet packet){
        Bukkit.getOnlinePlayers().forEach(player -> ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet));
    }

}
