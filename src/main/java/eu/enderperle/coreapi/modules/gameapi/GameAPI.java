package eu.enderperle.coreapi.modules.gameapi;

import eu.enderperle.coreapi.modules.hotbarmessager.HotbarMessager;
import eu.enderperle.coreapi.modules.titleapi.TitleAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Random;

public class GameAPI {

    public GameAPI(){}

    public void kickRandomPlayer(String permissionNotKickable, String kickmessage){
        HashMap<Integer, Player> players = new HashMap<>();
        int current = 0;
        for(Player all : Bukkit.getOnlinePlayers()){
            if(!all.hasPermission(permissionNotKickable)){
                players.put(current, all);
                current++;
            }
        }
        Random random = new Random();
        int randomPlayer = random.nextInt(current);
        Player Opfer = players.get(randomPlayer);
        Opfer.kickPlayer(kickmessage);

    }

    public void sendMessageToHotbar(Player player, String message){
        HotbarMessager.sendHotBarMessage(player, message);
    }
    public void sendTitlePlayer(Player player, String title, String subtitle, int fadeIn, int stay, int fadeout){
        TitleAPI api = new TitleAPI();
        api.setTitle(title);
        api.setSubtitle(subtitle);
        api.setFadeInTime(fadeIn);
        api.setFadeOutTime(fadeout);
        api.setStayTime(stay);
        api.setTimingsToSeconds();
        api.send(player);
    }



}
