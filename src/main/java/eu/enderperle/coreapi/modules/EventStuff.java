package eu.enderperle.coreapi.modules;

import eu.enderperle.coreapi.core.commands.Vanish;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventStuff implements Listener {

    @EventHandler
    public void on(PlayerJoinEvent event){
        Player joined = event.getPlayer();
        if(new Vanish().vanishedPeople.isEmpty()){return;}
        for(Player onlinevanished : new Vanish().vanishedPeople){
            CraftPlayer cp = ((CraftPlayer)onlinevanished);
            PacketPlayOutPlayerInfo removeTab = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
            PacketPlayOutEntityDestroy vanish = new PacketPlayOutEntityDestroy(cp.getEntityId());
            PacketPlayOutNamedEntitySpawn showVanished = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
            if(!joined.hasPermission("system.vanish")){
                ((CraftPlayer)joined).getHandle().playerConnection.sendPacket(removeTab);
                ((CraftPlayer)joined).getHandle().playerConnection.sendPacket(vanish);
            }else{
                ((CraftPlayer)joined).getHandle().playerConnection.sendPacket(removeTab);
                ((CraftPlayer)joined).getHandle().playerConnection.sendPacket(vanish);
                ((CraftPlayer)joined).getHandle().playerConnection.sendPacket(showVanished);
            }
        }

    }

    @EventHandler
    public void on(PlayerQuitEvent event){
        if(new Vanish(event.getPlayer()).isVanished()){
            new Vanish(event.getPlayer()).setVanished(false);
        }
    }

}
