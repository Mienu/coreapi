package eu.enderperle.coreapi.modules.filemanager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileManager {

    private String path;
    private String filename;
    private File file;
    private FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

    public FileManager(String path, String filename){
        this.path = path;
        this.filename = filename;
    }

    public void createFile(){
        file = new File(path, filename);
        if(!file.exists()){
            file.mkdir();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void save(){
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addtoFile(String value, String key){
        cfg.set(value, key);
        save();
    }
    public String getfromFile(String value){
        if(cfg.contains(value)){
            return cfg.getString(value);
        }
        return null;
    }

    public Location getLocationfromFile(String locname){
        if(!cfg.contains("loc." + locname + ".world")){
            return null;
        }
        World world;
        double x, y, z;
        float yaw, pitch;
        world = Bukkit.getWorld(cfg.getString("loc." + locname + ".world"));
        x = cfg.getDouble("loc." + locname + ".x");
        y = cfg.getDouble("loc." + locname + ".y");
        z = cfg.getDouble("loc." + locname + ".z");
        yaw = cfg.getInt("loc." + locname + ".x");
        pitch = cfg.getInt("loc." + locname + ".x");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public void addLocationtoFile(String locname, Location location){
        double x, y, z;
        float yaw, pitch;
        x = location.getX();
        y = location.getY();
        z = location.getZ();
        yaw = location.getYaw();
        pitch = location.getPitch();
        cfg.set("loc." + locname + ".world", location.getWorld().getName());
        cfg.set("loc." + locname + ".x", x);
        cfg.set("loc." + locname + ".y", y);
        cfg.set("loc." + locname + ".z", z);
        cfg.set("loc." + locname + ".yaw", yaw);
        cfg.set("loc." + locname + ".pitch", pitch);
        save();
    }

}
