package eu.enderperle.coreapi.core.commands;

import eu.enderperle.coreapi.modules.rangapi.RangAPI;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Set;

public class Vanish implements CommandExecutor {

    private HashMap<Player, Boolean> vanished = new HashMap<>();
    private Player player;
    private CraftPlayer cp;

    public Vanish(Player player){
        this.player = player;
        cp = ((CraftPlayer)player);
    }

    public Vanish(){}

    //Test

    public Set<Player> vanishedPeople = vanished.keySet();

    public boolean isVanished(){ return vanished.get(player); }

    public void setVanished(Boolean vanished){
        this.vanished.put(player, vanished);

        if(vanished){
            PacketPlayOutPlayerInfo tabremove = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
            PacketPlayOutEntityDestroy vanish = new PacketPlayOutEntityDestroy(cp.getEntityId());
            PacketPlayOutNamedEntitySpawn showVanished = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
            Bukkit.getOnlinePlayers().forEach(all -> {
                ((CraftPlayer)all).getHandle().playerConnection.sendPacket(tabremove);
                if(!all.hasPermission("system.vanish")){
                    ((CraftPlayer) all).getHandle().playerConnection.sendPacket(vanish);
                    return;
                }else{
                    new RangAPI(player).setRang();
                    ((CraftPlayer) all).getHandle().playerConnection.sendPacket(showVanished);
                }
            });
        }else{
            PacketPlayOutPlayerInfo tabremove = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
            PacketPlayOutNamedEntitySpawn show = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
            Bukkit.getOnlinePlayers().forEach(all -> {
               if(all.hasPermission("system.vanish")){
                   ((CraftPlayer)all).getHandle().playerConnection.sendPacket(tabremove);
               }
                new RangAPI(player).setRang();
                ((CraftPlayer)all).getHandle().playerConnection.sendPacket(show);
            });
        }


    }

    @Override
    public boolean onCommand(CommandSender cs, Command command, String s, String[] args) {

        if(!(cs instanceof Player)){return false;}
        Player p = (Player)cs;
        if(!p.hasPermission("system.vanish")){ p.sendMessage("§6Core §7- §cDazu bist du nicht berechtigt§7!"); return false; }

        new Vanish(p);

        if(isVanished()){
            setVanished(false);
            p.sendMessage("§6Core §7- §aDu bist nun im Vanish§7.");
            return false;
        }
        setVanished(true);
        p.sendMessage("§6Core §7- §cDu bist nun nicht mehr im Vanish§7-");
        return false;
    }
}
